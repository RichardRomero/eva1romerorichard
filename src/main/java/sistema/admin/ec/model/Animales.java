package sistema.admin.ec.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "persona")
public class Animales implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPersona;
	
	
	@Column(name = "nombres", nullable = false, length = 50)
	private String nombres;
	
	@Column(name = "raza", nullable = false, length = 50)
	private String raza;
	
	@Column(name = "sexo", nullable = false, length = 1)
	private String sexo;
	
	@Column(name ="pais", nullable = false, length = 30)
	private String pais;
	
	@Column(name = "direccion", nullable = false, length = 150)
	private String direccion;
	
	@Column(name = "foto", nullable = true)
	private byte[] foto;

}
