package sistema.admin.ec.service;

import sistema.admin.ec.model.Usuario;

public interface IUsuarioService extends IService<Usuario> {
	Usuario login(Usuario us);
}
