package sistema.admin.ec.service.Impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import sistema.admin.ec.dao.IRolDAO;
import sistema.admin.ec.model.Rol;
import sistema.admin.ec.model.Usuario;
import sistema.admin.ec.service.IRolService;



@Named
public class RolServiceImpl implements IRolService, Serializable{

	@EJB
	private IRolDAO dao;
	@Override
	public Integer registrar(Rol t) throws Exception {
		return dao.registrar(t);
	}

	@Override
	public Integer modificar(Rol t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer eliminar(Rol t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Rol listarPorId(Rol t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer asignar(Usuario us, List<Rol> roles) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Rol> listar() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
