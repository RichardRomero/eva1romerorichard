package sistema.admin.ec.service;

import java.util.List;

import sistema.admin.ec.model.Rol;
import sistema.admin.ec.model.Usuario;

public interface IRolService extends IService<Rol>{
	Integer asignar(Usuario us, List<Rol> roles);
}
