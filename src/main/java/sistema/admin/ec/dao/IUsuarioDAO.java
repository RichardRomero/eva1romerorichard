package sistema.admin.ec.dao;

import sistema.admin.ec.model.Usuario;

public interface IUsuarioDAO extends ICRUD<Usuario>{
	String traerPassHasched(String us);
	Usuario leerPorNombreUsuario(String nombre);
}
