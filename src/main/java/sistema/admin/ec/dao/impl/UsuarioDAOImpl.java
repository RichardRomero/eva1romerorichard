package sistema.admin.ec.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import sistema.admin.ec.dao.IUsuarioDAO;
import sistema.admin.ec.model.Usuario;



@Stateless
public class UsuarioDAOImpl implements IUsuarioDAO, Serializable{
	
	@PersistenceContext(unitName = "aniPU")
	private EntityManager em;
	
	@Override
	public Integer registrar(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		em.persist(t);
		return t.getPersona().getIdPersona();
	}

	@Override
	public Integer modificar(Usuario t) throws Exception {
		em.merge(t);
		return t.getPersona().getIdPersona();
	}

	@Override
	public Integer eliminar(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		em.remove(t);
		return 1;
	}

	@Override
	public Usuario listarPorId(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Usuario leerPorNombreUsuario(String nombre) {
		Usuario usuario = new Usuario();
		try {
			Query query = em.createQuery("FROM Usuario u WHERE u.usuario = ?1");
			query.setParameter(1, nombre);
			
			List<Usuario> lista = (List<Usuario>) query.getResultList();
			if (!lista.isEmpty()) {
				usuario = lista.get(0);
			}
			
		} catch (Exception e) {
			throw e;
		}
		return usuario;
	}

	@Override
	public List<Usuario> listar() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String traerPassHasched(String us) {
		Usuario usuario = new Usuario();
		try {
			// SQL select clave from usuario where usuario = 'santiago'
			Query query = em.createQuery("FROM Usuario u WHERE u.usuario = ?1");
			query.setParameter(1, us);
			
			List<Usuario> lista = (List<Usuario>) query.getResultList();
			if (!lista.isEmpty()) {
				usuario = lista.get(0);
			}
		} catch (Exception e) {
			throw e;
		}
		return usuario != null && usuario.getId() != null ? usuario.getClave() : "";
	}
	}
	

