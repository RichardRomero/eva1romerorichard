package sistema.admin.ec.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateful;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContexts;
import javax.persistence.Query;

import sistema.admin.ec.dao.IPersonaDAO;
import sistema.admin.ec.model.Persona;



//@Named
@Stateful
public class PersonaDAOImpl implements IPersonaDAO, Serializable{
	
	@PersistenceContext (unitName = "aniPU")
	private EntityManager em;
	


	@Override
	public Integer registrar(Persona t) throws Exception {
		em.persist(t);
		return t.getIdPersona();
	}

	@Override
	public Integer modificar(Persona t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer eliminar(Persona t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Persona listarPorId(Persona t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Persona> listar() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
}
