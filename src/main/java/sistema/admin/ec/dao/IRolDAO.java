package sistema.admin.ec.dao;

import java.util.List;

import javax.ejb.Local;

import sistema.admin.ec.model.Rol;
import sistema.admin.ec.model.Usuario;
import sistema.admin.ec.model.UsuarioRol;



@Local
public interface IRolDAO extends ICRUD<Rol>{
	Integer asignar(Usuario us, List<UsuarioRol> roles);
}
