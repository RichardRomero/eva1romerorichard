package sistema.admin.ec.dao;

import java.util.List;

import javax.ejb.Local;

import sistema.admin.ec.model.Persona;



//en contexto de la misma maquina virtual
@Local
public interface IPersonaDAO extends ICRUD<Persona>{
	
}
