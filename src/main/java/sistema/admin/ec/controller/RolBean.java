package sistema.admin.ec.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.RowEditEvent;

import sistema.admin.ec.model.Rol;
import sistema.admin.ec.service.IRolService;


@Named
@ViewScoped
public class RolBean implements Serializable{
	
	@Inject
	private IRolService service;
	
	private List<Rol> lista;

	public List<Rol> getLista() {
		return lista;
	}

	public void setLista(List<Rol> lista) {
		this.lista = lista;
	}

	@PostConstruct
	public void init() {
		this.llenarTabla();
	}
	
	public void llenarTabla() {
		try {
			this.lista = this.service.listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void onRowEdit(RowEditEvent event) {
		try {
			this.service.modificar((Rol)event.getObject());
			FacesMessage msg = new FacesMessage("Rol Editado",((Rol) event.getObject()).getTipo());
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
