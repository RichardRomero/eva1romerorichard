package sistema.admin.ec.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import sistema.admin.ec.model.Animales;
import sistema.admin.ec.service.IAnimalService;

@Named
@ViewScoped
public class AnimalBean implements Serializable{
	@Inject
	private IAnimalService service;
	private Animales animales;
	private List<Animales> lista;
	public Animales getAnimales() {
		return animales;
	}
	public void setAnimales(Animales animales) {
		this.animales = animales;
	}
	public List<Animales> getLista() {
		return lista;
	}
	public void setLista(List<Animales> lista) {
		this.lista = lista;
	}
	@PostConstruct
	public void init()
	{
		
	}
	public void listar()
	{
		
	}

	// private String tipoDialogo;

}
