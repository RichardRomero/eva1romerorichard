package sistema.admin.ec.controller;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Query;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.RowEditEvent;

import sistema.admin.ec.model.Persona;
import sistema.admin.ec.model.Usuario;
import sistema.admin.ec.service.IPersonaService;



@Named
@ViewScoped
public class PersonaBean implements Serializable {
	@Inject
	private IPersonaService service;
	private Persona persona;
	private List<Persona> lista;
	private Usuario us;
	private String tipoDialogo;

	public List<Persona> getLista() {
		return lista;
	}

	public void setLista(List<Persona> lista) {
		this.lista = lista;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	/*
	 * public PersonaBean() { this.persona = new Persona(); //this.service = new
	 * PersonaServiceImpl(); this.listar(); }
	 */

	@PostConstruct
	public void init() {
		FacesContext context = FacesContext.getCurrentInstance();
		this.us = (Usuario) context.getExternalContext().getSessionMap().get("usuario");
		this.persona = new Persona();
		this.listar();
	}

	// metodos
	public void operar(String accion) {
		try {
			if (accion.equalsIgnoreCase("R")) {
				this.service.registrar(persona);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Persona Registrada",
						"Bienvenido"));
			} else if (accion.equalsIgnoreCase("M")) {
				this.service.modificar(persona);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Operación Exitosa", 
						"Persona Modificada"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void handleFileUpload(FileUploadEvent event) {
		try {
			if (event.getFile() != null) {
				this.persona.setFoto(event.getFile().getContents());
			}
		} catch (Exception e) {

		}
	}

	public void mostrarData(Persona p) {
		this.persona = p;
		this.tipoDialogo = "Modificar Persona";
	}

	public void listar() {
		try {
			this.lista = this.service.listar();
			this.lista.remove(this.us.getPersona());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void limpiarControles() {
		this.persona = new Persona();
		this.tipoDialogo = "Nueva Persona";
	}

	public String getTipoDialogo() {
		return tipoDialogo;
	}

	public void setTipoDialogo(String tipoDialogo) {
		this.tipoDialogo = tipoDialogo;
	}

}
