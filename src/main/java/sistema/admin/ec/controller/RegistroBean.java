package sistema.admin.ec.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.mindrot.jbcrypt.BCrypt;

import sistema.admin.ec.model.Persona;
import sistema.admin.ec.model.Usuario;
import sistema.admin.ec.service.IPersonaService;
import sistema.admin.ec.service.IRolService;

@Named
@ViewScoped
public class RegistroBean implements Serializable{
	
	@Inject
	private IPersonaService personaService;
	
	@Inject
	private IRolService rolService;
	
	private Persona persona;
	private Usuario usuario;
	
	@PostConstruct
	private void init() {
		this.persona = new Persona();
		this.usuario = new Usuario();
	}
	
	@Transactional
	public String registrar() {
		String redireccion = "";
		try {
			String clave = this.usuario.getClave();
			String claveHash = BCrypt.hashpw(clave, BCrypt.gensalt());
			this.usuario.setClave(claveHash);
			this.usuario.setPersona(persona);
			this.persona.setUsu(usuario);
			this.personaService.registrar(persona);
			redireccion = "index?faces-redirect=true";
		} catch (Exception e) {
			
		}
		return redireccion;
	}
	
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
}
